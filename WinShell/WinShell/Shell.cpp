#include "Shell.h"

void Shell::errorPrint()
{
	//Get the error message, if any.
	DWORD errorMessageID = ::GetLastError();
	if (errorMessageID == 0)
		return;
	LPSTR messageBuffer = nullptr;
	size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

	std::string message(messageBuffer, size);

	//Free the buffer.
	LocalFree(messageBuffer);

	cout << message;
}

Shell::Shell()
{
}


Shell::~Shell()
{
}

void Shell::run()
{
	cout << "Welcome to WinShell\n";
	while(true)
	{
		cout << ">>  ";
		string in; getline(cin, in);
		if (!in.length())
			continue;
		bool res = true;
		auto input = Helper::get_words(in);
		if (input[0] == "pwd")
		{
			pwd();
			res = true;
		}
		else if (input[0] == "cd")
		{
			if (input.size() > 1)
				res = cd(input[1]);
			else
				cout << "No destination specified" << endl;
		}
		else if (input[0] == "create")
			res = create(input[1]);
		else if (input[0] == "ls")
			ls();
		else if (input[0] == "secret")
			res = secret();
		else if (input[0] == "executable")
			res = exe(input);
		else
		{
				cout << "Command is not supported"<<endl;
				continue;
		}
		if (!res)
			this->errorPrint();

	}

}

void Shell::pwd()
{
	char buf[PATH_MAX] = "";
	GetCurrentDirectory(PATH_MAX,buf);
	std::cout << buf << std::endl;
}

bool Shell::exe(vector<string> arg)
{
	if (arg.size() > 1)
	{
		STARTUPINFO si;
		PROCESS_INFORMATION pi;
		ZeroMemory(&si, sizeof(si));
		si.cb = sizeof(si);
		ZeroMemory(&pi, sizeof(pi));
		string arguments = "";
		if (arg.size() > 2)
		{
			for (auto i = arg.begin() + 1; i != arg.end(); i++)
				arg[1] += *i;
		}
		DWORD exit;
		try
		{
			if(!CreateProcess((arg[1]).c_str(), NULL,NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
						return false;
		}
		catch (std::exception& ex)
		{
			return false;
		}
		WaitForSingleObject(pi.hProcess, INFINITE);
		GetExitCodeProcess(pi.hProcess, &exit);
		cout << "Exit with code  " << exit << endl;
		return true;
	}
	cout << "No filename written\n";
	return true;

}

bool Shell::cd(string second)
{
	return SetCurrentDirectory(second.c_str()) != 0;
}

bool Shell::create(string what)
{
	HANDLE t = CreateFileA(what.c_str(), FILE_READ_DATA, FILE_SHARE_READ, NULL, OPEN_ALWAYS, 0, NULL);
	if (t == INVALID_HANDLE_VALUE)
		return false;
	CloseHandle(t);
	return true;
}

void Shell::ls()
{
	WIN32_FIND_DATA search;
	char buf[PATH_MAX] = "";
	GetCurrentDirectory(PATH_MAX, buf);
	string t = string(buf) + "/*.*";
	HANDLE hfind = FindFirstFile(t.c_str(), &search);
	if (hfind != INVALID_HANDLE_VALUE)
	{
		do
		{
			cout << search.cFileName << endl;
		} while (FindNextFile(hfind,&search));
	}
}

bool Shell::secret()
{
	HMODULE dll = LoadLibraryA(TEXT("Secret.dll"));
	if (GetLastError() != ERROR_SUCCESS)
	{
		return false;
	}
	int(*fun)() = (int(*)())GetProcAddress(dll, "TheAnswerToLifeTheUniverseAndEverything");
	if (GetLastError()==ERROR_SUCCESS) {
		cout <<"is says: "<< fun()<<endl;
		FreeLibrary(dll);
		return true;
	}
	else
	{
		return false;
	}
}

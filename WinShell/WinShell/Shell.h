#pragma once
#include<Windows.h>
#include "Helper.h"
#define PATH_MAX 2024
#include<iostream>
#include <string>
using namespace std;
class Shell
{
public:
	
	Shell();
	~Shell();
	void run();
private:
	void errorPrint();
	void pwd();

public:
	bool exe(vector<string> arg);
private:
	bool cd(string second);
	bool create(string what);
	void ls();
	bool secret();
};

